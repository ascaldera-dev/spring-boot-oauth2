## About

This is an example project that illustrates creating a RESTful API in Spring Boot.

## Runnning this project

```
mvn spring-boot:run
```

## Get token

```
curl -X POST --user 'gigy:secret' -d 'grant_type=password&username=test@test.com&password=password' http://localhost:8000/gigy/oauth/token
```
If you are using Postman app you have to add header:
```
Authorization: Basic Z2lneTpzZWNyZXQ=
```
where `Z2lneTpzZWNyZXQ=` is a combination of `gigy:secret` base64-encoded
## Example commands

Getting all people from the API:
```
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $TOKEN" -X GET http://localhost:8000/gigy/people
```

## Access H2 database from browser

Open web browser and go to:
```
http://localhost:8000/gigy/h2-console
```
Login credentials:
```
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:~/test
User Name: sa
Password: *blank*
```

## Migrations (Liquibase)

Migrations run automatically when booting application  

Each migration file is named `db.changelog-1.X.X.xml`  

Initial migration: `db.changelog-1.0.0.xml`  

All migration files are included inside `db.changelog-master.xml` file  

To create new migration you have to:

* change `diffChangeLogFile` property inside `liquibase.properties` to a newer file version. For example `diffChangeLogFile=src/main/resources/db/changelog/db.changelog-1.0.1.xml`
* run `mvn liquibase:diff` command
* include new migration file (for example: `db.changelog-1.0.1.xml`) inside `db.changelog-master.xml` (**autoincrement upside down**)
* run `mvn liquibase:update` to implement new changes to your database
* check if all the changes were implemented correctly to your database

## Unit testing (JUnit)

To run all tests run `mvn test` from root project directory

## LICENSE

The code is released under the Apache License 2.0. See LICENSE for details.
