package com.gigy.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gigy.model.Instructor;
import com.gigy.repository.InstructorRepository;

@RestController
@RequestMapping("/instructors")
public class InstructorController{
	
	@Autowired
	private InstructorRepository instructorRepo;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<Instructor>> getInstructors(){
		return new ResponseEntity<>(instructorRepo.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Instructor> getInstructor(@PathVariable long id){
		Instructor instructor=instructorRepo.findOne(id);
		
		if(instructor!=null) {
			return new ResponseEntity<>(instructorRepo.findOne(id),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/{id}",method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteInstructor(@PathVariable long id){
		instructorRepo.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> addInstructor(@RequestBody Instructor instructor) {
		return new ResponseEntity<>(instructorRepo.save(instructor), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Instructor> updateInstructor(@PathVariable long id, @RequestBody Instructor instructor){
		Instructor currentInstructor= instructorRepo.findOne(id);
		currentInstructor.setFirstName(instructor.getFirstName());
		currentInstructor.setLastName(instructor.getLastName());
		currentInstructor.setYearsOfExperience(instructor.getYearsOfExperience());
		
		return new ResponseEntity<>(instructorRepo.save(currentInstructor), HttpStatus.OK);
	}
	
}
