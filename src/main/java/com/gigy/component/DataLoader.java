package com.gigy.component;




import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.repository.query.parser.Part;
import org.springframework.stereotype.Component;

import com.gigy.model.Instructor;
import com.gigy.model.Party;
import com.gigy.model.Person;
import com.gigy.model.User;
import com.gigy.repository.InstructorRepository;
import com.gigy.repository.PartyRepository;
import com.gigy.repository.PersonRepository;
import com.gigy.repository.UserRepository;

@Component
public class DataLoader implements ApplicationRunner {

    private UserRepository userRepository;
    private PersonRepository personRepository;
    private InstructorRepository instructorRepo;
    private PartyRepository partyRepository;

    @Autowired
    public DataLoader(UserRepository userRepository,PersonRepository personRepository, InstructorRepository instructorRepo, PartyRepository partyRepository) {
        this.userRepository = userRepository;
        this.personRepository = personRepository;
        this.instructorRepo = instructorRepo;
        this.partyRepository = partyRepository;
    }

    public void run(ApplicationArguments args) {
        userRepository.save(new User(1L, "test@test.com","$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri", true));
        userRepository.save(new User(2L, "matic@example.com","$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri", true));
        userRepository.save(new User(3L, "katie@example.com","$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri", true));
        userRepository.save(new User(4L, "john@example.com", "$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri", true));
        userRepository.save(new User(5L, "michael@example.com", "$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri", true));
        personRepository.save(new Person(1L,"test","test@test.com",35));
        personRepository.save(new Person(2L,"matic","matic@example.com",21));
        personRepository.save(new Person(3L,"katia","katie@example.com",31));
        personRepository.save(new Person(4L,"john","john@example.com",54));
        personRepository.save(new Person(5L,"michael","michael@example.com",54));
        instructorRepo.save(new Instructor(1L,"matic","tempfer",3));
        instructorRepo.save(new Instructor(1L,"john","john",21));
        instructorRepo.save(new Instructor(1L,"test","test",13));
        partyRepository.save(new Party(1L, "Garden", new Date()));
        
    }
}
