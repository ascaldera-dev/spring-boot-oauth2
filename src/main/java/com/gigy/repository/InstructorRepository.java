package com.gigy.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gigy.model.Instructor;


@Repository
public interface InstructorRepository extends CrudRepository<Instructor, Long> {
	
	Collection<Instructor> findAll();
	
	Instructor findByFirstName(String firstName);
	Instructor findById(long id);

}
