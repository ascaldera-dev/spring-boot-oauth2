package com.gigy.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.filters.CorsFilter;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import com.gigy.model.Instructor;
import com.gigy.repository.InstructorRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = InstructorController.class, secure = false)
public class InstructorControllerTest{
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean
	private InstructorRepository instructorRepo;
	
	private Instructor instructor;
	
	@Before
	public void prepare() {
		instructor = new Instructor();
		instructor.setId(1l);
		instructor.setFirstName("matic");
		instructor.setLastName("tempfer");
		instructor.setYearsOfExperience(3);
	}
	
	@Test
	public void getInstructorsTest() throws Exception {
		List<Instructor> instructors = new ArrayList<Instructor>();
		instructors.add(instructor);
		
		given(instructorRepo.findAll()).willReturn(instructors);
		mvc.perform(get("/instructors")
			.accept(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].id", is(1)))
			.andExpect(jsonPath("$[0].firstName", is("matic")))
			.andExpect(jsonPath("$[0].lastName", is("tempfer")))
			.andExpect(jsonPath("$[0].yearsOfExperience", is(3)));;
	}
	
	@Test
	public void createInstructorTest() throws Exception {
		instructor = new Instructor();
		instructor.setId(2l);
		instructor.setFirstName("testFirstNew");
		instructor.setLastName("testLastNew");
		instructor.setYearsOfExperience(3);
		
		mvc.perform(post("/instructors")
				.accept(MediaType.APPLICATION_JSON)
				.content(this.mapToJson(instructor))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated());
	}
	
	@Test
	public void getInstructorTest() throws Exception {
		given(instructorRepo.findOne(1l)).willReturn(instructor);
		
		mvc.perform(get("/instructors/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.id", is(1)))
			.andExpect(jsonPath("$.firstName", is("matic")))
			.andExpect(jsonPath("$.lastName", is("tempfer")))
			.andExpect(jsonPath("$.yearsOfExperience", is(3)));
	}
	
	@Test
	public void deleteInstructorTest() throws Exception {
		
		mvc.perform(delete("/instructors/1")
		.accept(MediaType.APPLICATION_JSON)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/*
     * converts a Java object into JSON representation
     */
	private String mapToJson(Object obj) {
		try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
}
