package com.gigy.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

import com.gigy.model.Instructor;
import com.gigy.model.Party;
import com.gigy.repository.PartyRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PartyController.class, secure = false)
public class PartyControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private PartyRepository partyRepo;
	
	private Party party;
	
	@Before
	public void prepare() {
		party = new Party();
		party.setId(1l);
		party.setLocation("Garden");
	}

	@Test
	public void getPartiesTest() throws Exception {
		List<Party> parties = new ArrayList<Party>();
		parties.add(party);
		
		given(partyRepo.findAll()).willReturn(parties);
		mvc.perform(get("/parties")
			.accept(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].id", is(1)))
			.andExpect(jsonPath("$[0].location", is("Garden")));
	}
	
	@Test
	public void createPartyTest() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
		Date d = sdf.parse("2018-02-20");
		
		party = new Party();
		party.setDate(d);
		party.setLocation("testLocation");
		
		mvc.perform(post("/parties")
			.accept(MediaType.APPLICATION_JSON)
			.content(this.mapToJson(party))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated());
	}
	
	@Test
	public void getPartyTest() throws Exception {
		given(partyRepo.findOne(1l)).willReturn(party);
		mvc.perform(get("/parties/1").accept(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.location", is("Garden")));
	}
	
	@Test
	public void partyNotFoundTest() throws Exception {
		mvc.perform(get("/parties/2")
			.accept(MediaType.APPLICATION_JSON_VALUE))
			.andExpect(status().isNotFound());
	}
	
	@Test
	public void deletePartyTest() throws Exception {
		
		mvc.perform(delete("/parties/1")
		.accept(MediaType.APPLICATION_JSON)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/*
     * converts a Java object into JSON representation
     */
	private String mapToJson(Object obj) {
		try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
	

}
