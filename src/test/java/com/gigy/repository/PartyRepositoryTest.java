package com.gigy.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.gigy.model.Party;

import static org.assertj.core.api.Assertions.*;

import java.util.Date;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PartyRepositoryTest {
	
    @Autowired
    private PartyRepository repository;
    
    @Test
    public void testSaveParty() throws Exception {
    	Party party = new Party();
    	party.setLocation("testLocation");
    	party.setDate(new Date());
    	Party partyOutput = this.repository.save(party);
        assertThat(partyOutput.getLocation()).isEqualTo("testLocation");
    }

}
